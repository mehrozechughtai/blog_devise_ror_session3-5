class PostsController < ApplicationController
  before_action :authenticate_user!
  
  def index
  	@posts = Post.order('created_at DESC')
  end

  def show

    @post = Post.find(params[:id])

  end

  def edit

    @post = Post.find(params[:id])


  end

  def new
  	@post = Post.new
  end
  def create

  	 @post = Post.new(user_params)

    if @post.save

      redirect_to posts_path, :notice => "Your Post was saved!"
    
    else
      render "new"

    end

  end
  def update

    @post = Post.find(params[:id])
    if @post.update_attributes(user_params)
      redirect_to posts_path, :notice => "Your Post has been updated"
    else
      render "edit"
    end

  end

  def destroy

    @post = Post.find(params[:id])

    @post.destroy
    redirect_to posts_path

  end


 private
 	def user_params
  	  params.require(:post).permit(:title, :content)
  	end
end
